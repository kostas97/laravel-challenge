<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voyages extends Model
{

    public function voyages(){
    	return $this->belongTo(Vessels::class);
    }
}
