<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

$id = 0;

class VoyageController extends Controller
{

	private function idCalc(){
		$id = $id + 1;
		return $id;
	}

    public function voyages(){
    	return view('voyages');
    }

    public function store(){
    	$data = request()->validate([
    		'Vessel Id'=>'required',
    		'Starting Date'=>'required',
    		'Ending Date'=>'required',
    		'Revenues'=>'required',
    		'Expenses'=>'required',
    	]);

	    $temp = DB::select('SELECT name FROM vessels WHERE id = ?', $data['Vessel Id']);
	    $str1 = $temp.$data['Starting Date'];
	    $str2 = "Submitted";
	    $t1 = $data['Revenues'];
	    $t2 = $data['Expenses'];
	    
	    $rev = number_format($t1,2);
	    $exp = number_format($t2,2);
	    $profit = $rev - $exp;

		DB::insert('INSERT INTO voyages (id, vessel_id, code, start_date, end_date, status, revenues, expenses, profit) VALUES (?,?,?,?,?,?,?,?,?)',idCalc(), $data['Vessel Id'], $str1, $data['Starting Date'], $data['Ending Date'], $str2, $rev, $exp, $profit);
    }
}
