<div class="container">

    <form action="/voyages" method="POST">
        <head>
            <meta name="csrf-token" content="{{ csrf_token() }}" />
        </head>
        <div class="row">
            <h1>Create New Voyage</h1>
        </div>

        <div class="row">
            <div class="col-8 offset-2">

                <div class="form-group row">
                    <label for="Vessel Id" class="col-md-4 col-form-label">Post Vessel Id</label>
                    <input id="Vessel Id" type="text" class="form-control @error('Vessel Id') is-invalid @enderror" name="Vessel Id">
                </div>
                <div class="form-group row">
                    <label for="Starting Date" class="col-md-4 col-form-label">Post Starting Date</label>
                    <input id="Starting Date" type="text" class="form-control @error('Starting Date') is-invalid @enderror" name="Starting Date">
                </div>
                <div class="form-group row">
                    <label for="Ending Date" class="col-md-4 col-form-label">Post Ending Date</label>
                    <input id="Ending Date" type="text" class="form-control @error('Ending Date') is-invalid @enderror" name="Ending Date">
                </div>
                <div class="form-group row">
                    <label for="Revenues" class="col-md-4 col-form-label">Post Revenues</label>
                    <input id="Revenues" type="text" class="form-control @error('Revenues') is-invalid @enderror" name="Revenues">
                </div>
                <div class="form-group row">
                    <label for="Expenses" class="col-md-4 col-form-label">Post Expenses</label>
                    <input id="Expenses" type="text" class="form-control @error('Expenses') is-invalid @enderror" name="Expenses">
                </div>

            </div>
        </div>
        <div class="row pt-4">
            <button class="btn btn-primary">Submit New Voyage</button>
        </div>
    </form>

</div>
